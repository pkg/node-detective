# Installation
> `npm install --save @types/detective`

# Summary
This package contains type definitions for detective (https://github.com/browserify/detective).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/detective.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 09:08:40 GMT
 * Dependencies: [@types/acorn](https://npmjs.com/package/@types/acorn)
 * Global values: none

# Credits
These definitions were written by [TeamworkGuy2](https://github.com/TeamworkGuy2).
